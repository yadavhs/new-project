<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    public function getPermissionIdsAttribute(){
        return  RolePermissions::where('role_id',$this->id)->get()->pluck('permission_id')->toArray();
    }
}

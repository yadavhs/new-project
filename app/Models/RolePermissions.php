<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolePermissions extends Model
{
    public  $table = 'permission_role';
    protected $fillable = [
        'role_id',
        'permission_id',

    ];
}

<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Permissions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class PermissionsController extends AdminController
{
    public function index(){
        if(checkPermission('admin.permissions.list')){
            $this->setDatatableJSCSS();
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Permissions',
                    'url'=> '#',
                    'active'=> true,

                ]
            ];

            return view('Admin.Auth.Permissions.index',[
                'page_title' => 'Permissions',
                'menu' => 'authentication',
                'submenu' => 'permissions',
                'breadcrumbData' => $breadcrumbData,
            ]);
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }
    public function datatableList(){
        return DataTables::eloquent(Permissions::query())
            ->addColumn('created',function ($query){
                return date('M d Y',strtotime($query->created_at));
            })
            ->addColumn('action',function ($query){
                return '<a class="btn btn-sm btn-primary-custom" href="'.route('permissions.edit',$query->id).'" title="Edit"><i class="fa fa-edit"></i></a>
                        ';
//                <button class="btn btn-sm btn-danger" onclick="deletePermission($(this),'.$query->id.')"><i class="fa fa-trash"></i></button>
            })
            ->toJson();
    }

    public function create(){
        if(checkPermission('admin.permissions.create')){
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Permissions',
                    'url'=> route('permissions'),
                    'active'=> false,

                ],
                [
                    'title' => 'Create',
                    'url'=>'#',
                    'active'=> true,

                ]

            ];

            return view('Admin.Auth.Permissions.create',[
                'page_title' => 'Create Permission',
                'menu' => 'authentication',
                'submenu' => 'permissions',
                'breadcrumbData' => $breadcrumbData,
            ]);
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }

    }
    public function edit($id){
        if(checkPermission('admin.permissions.update')){
            if($permission = Permissions::find($id)){
                $breadcrumbData = [
                    [
                        'title'=>'Dashboard',
                        'url' => route('dashboard'),
                        'active' => false,
                    ],
                    [
                        'title' => 'Roles',
                        'url'=> route('permissions'),
                        'active'=> false,

                    ],
                    [
                        'title' => 'Edit',
                        'url'=>'#',
                        'active'=> true,

                    ]

                ];
                return view('Admin.Auth.Permissions.create',[
                    'page_title' => 'Update Permission',
                    'menu' => 'authentication',
                    'submenu' => 'permissions',
                    'breadcrumbData' => $breadcrumbData,
                    'permission' => $permission,
                ]);
            }else{
                return Redirect::back()->with(['msg_type' => 'success','msg' => 'Item was not found']);
            }
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }
    public function save(Request $request,$id =null){
        if($id){
            $permission = Permissions::find($id);
            if($request->name){
                $permission->update(['name'=> $request->name]);
            }

            if($request->description){
                $permission->update(['description'=> $request->description]);
            }

            return redirect('admin/permission')->with(['msg_type'=>'success','msg'=>'Permission Updated']);
        }else{
            Permissions::create($request->all());
            return redirect('admin/permission')->with(['msg_type'=>'success','msg'=>'Permission Created']);
        }

    }
    public function delete($id){
        $response = [];
        if($permission = Permissions::find($id)){
            $permission->delete();
            $response = [
                'status' => 1,
                'message' => 'Item deleted',
            ];
        }else{
            $response= [
                'status' => 0,
                'error' => 'Item was not founded'
            ];
        }
        return $response;
    }
}

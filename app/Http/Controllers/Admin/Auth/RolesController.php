<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\PermissionRoles;
use App\Models\Permissions;
use App\Models\Role;
use App\Models\RolePermissions;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class RolesController extends AdminController
{
    public function index()
    {
        if (checkPermission('admin.roles.list')){
            $this->setDatatableJSCSS();
        $breadcrumbData = [
            [
                'title' => 'Dashboard',
                'url' => route('dashboard'),
                'active' => false,
            ],
            [
                'title' => 'Roles',
                'url' => route('roles'),
                'active' => true,
            ]

        ];
        return view('Admin.Auth.Roles.index', [
            'page_title' => 'Roles',
            'menu' => 'authentication',
            'submenu' => 'roles',
            'breadcrumbData' => $breadcrumbData,
        ]);
    }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);

        }
    }
    public function datatableList(){
        return DataTables::eloquent(Roles::query())
            ->addColumn('created',function ($query){
                return date('M d Y',strtotime($query->created_at));
            })
            ->addColumn('action',function ($query){
                $links = '';
                if(checkPermission('admin.roles.edit')) {
                    $links .= '<a class="btn btn-sm btn-primary-custom" href="'.route('roles.edit',$query->id).'" title="Edit"><i class="fa fa-edit"></i></a>';
                }
                if(checkPermission('admin.roles.delete')) {
                    $links .= '&nbsp;&nbsp;<button class="btn btn-sm btn-danger" onclick="deleteRole($(this),'.$query->id.')"><i class="fa fa-trash"></i></button>';
                }
                return $links;
            })
            ->toJson();
    }

    public function create(){
        if(checkPermission('admin.roles.create')){
            $this->loadSelect2();
            $breadcrumbData = [
                [
                    'title'=>'Dashboard',
                    'url' => route('dashboard'),
                    'active' => false,
                ],
                [
                    'title' => 'Roles',
                    'url'=> route('roles'),
                    'active'=> false,

                ],
                [
                    'title' => 'Create',
                    'url'=>'#',
                    'active'=> true,

                ]

            ];

            return view('Admin.Auth.Roles.create',[
                'page_title' => 'Create Role',
                'menu' => 'authentication',
                'submenu' => 'roles',
                'breadcrumbData' => $breadcrumbData,
                'permissions' => Permissions::all()->pluck('description','id'),
            ]);

        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }

    }
    public function edit($id){
        if(checkPermission('admin.roles.edit')){

            if($role = Roles::find($id)){
                $this->loadSelect2();
                $breadcrumbData = [
                    [
                        'title'=>'Dashboard',
                        'url' => route('dashboard'),
                        'active' => false,
                    ],
                    [
                        'title' => 'Roles',
                        'url'=> route('roles'),
                        'active'=> false,

                    ],
                    [
                        'title' => 'Edit',
                        'url'=>'#',
                        'active'=> true,

                    ]

                ];
                return view('Admin.Auth.Roles.create',[
                    'page_title' => 'Update Role',
                    'menu' => 'authentication',
                    'submenu' => 'roles',
                    'breadcrumbData' => $breadcrumbData,
                    'role' => $role,
                    'permissions' => Permissions::all()->pluck('description','id'),
                ]);
            }else{
                return Redirect::back()->with(['msg_type' => 'success','msg' => 'Item was not found']);
            }
        }else{
            return Redirect::back()->with(['msg_type'=>'error','msg'=>'you are not allowed to access']);
        }
    }

    public function save(Request $request,$id = null){
        if($id){
            $role = Roles::find($id);
            if($request->name){
                $role->update(['name' => $request->name]);
            }
            if($request->description){
                $role->update(['description' => $request->description]);
            }
            if($request->permissions){
                RolePermissions::where('role_id',$id)->delete();
                foreach ($request->permissions as $permission_id){

                    RolePermissions::create(['permission_id'=> $permission_id,'role_id'=>$id]);
                }
            }
            return redirect('admin/role/')->with(['msg_type'=>'success','msg' => 'Role Updated']);

        }else{
            $role =   Roles::create($request->all());
            if($request->permissions) {
                RolePermissions::where('role_id', $role->id)->delete();
                foreach ($request->permissions as $permission_id) {
                    RolePermissions::create(['permission_id' => $permission_id, 'role_id' => $role->id]);
                }
            }
            return redirect('admin/role/')->with(['msg_type'=>'success','msg' => 'Role Created']);
        }
    }
    public function delete($id){
        $response = [];
        if($role = Roles::find($id)){
            $role->delete();
            $response = [
                'status' => 1,
                'msg' => 'Item deleted'
            ];
        }else{
            $response = [
                'status' => 0,
                'msg' => 'Item was not found'
            ];
        }
        return response($response);
    }
}

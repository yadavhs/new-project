<?php
use \Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;


function uploadImage($image, $directory, $filename, $width = null, $height = null){
    if (!is_null($image) && $image->isValid()) {
        if (!File::isDirectory($directory)) {
            File::makeDirectory($directory, 0755, true);
        }

        $image = Image::make($image->getRealpath());
//        $image->orientate();

        if ($width || $height) {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image->save($directory . '/' . $filename);
    }
}

function regenerateThumbnail($image, $directory, $filename, $width = null, $height = null){
    if (!is_null($image) && $image->isValid()) {
        if (!File::isDirectory($directory)) {
            File::makeDirectory($directory, 0755, true);
        }

        $image = Image::make($image->getRealpath());
//        $image->orientate();

        if ($width || $height) {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image->save($directory . '/' . $filename);
    }
}


function uploadImageFromUrl($imageURL, $directory, $filename, $width = null, $height = null)
{
    if (!File::isDirectory($directory)) {
        File::makeDirectory($directory, 0755, true);
    }

    /* Create default size */
    Image::make($imageURL)
        ->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->save($directory . '/' . $filename);
}

function checkPermission($permission)
{

    $currentUser = \Illuminate\Support\Facades\Auth::user();
    if($permission = \App\Models\Permissions::where('name',$permission)->first()){
        $status = \App\Models\RolePermissions::where('permission_id',$permission->id)->whereIn('role_id',$currentUser->role_ids)->get();
        if($status->count() > 0){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

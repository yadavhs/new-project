<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('/login','Admin\Auth\UsersController@index');
Route::post('/postLogin','Admin\Auth\UsersController@postLogin')->name('postLogin');
Route::group([ 'middleware' => \App\Http\Middleware\RedirectIfAuthenticated::class],function() {
    Route::get('logout','Admin\Auth\UsersController@logout')->name('logout');
    Route::get('dashboard','Admin\Dashboard\DashboardController@index')->name('dashboard');
    Route::get('/','Admin\Dashboard\DashboardController@index');
    Route::group(['middleware' => \App\Http\Middleware\DisablePreventLogout::class], function () {
        Route::group(['prefix' => 'admin-users'], function () {
            Route::get('/', 'Admin\Auth\UsersController@userIndex')->name('adminUsers');
            Route::get('/datatableList', 'Admin\Auth\UsersController@userList')->name('adminUserDatatableList');
            Route::get('/create', 'Admin\Auth\UsersController@create')->name('adminUsers.create');
            Route::post('/save/{id?}', 'Admin\Auth\UsersController@save')->name('adminUsers.save');
            Route::get('/view/{id}', 'Admin\Auth\UsersController@view')->name('adminUsers.view');
            Route::get('/edit/{id}', 'Admin\Auth\UsersController@edit')->name('adminUsers.edit');
            Route::get('/delete/{id}', 'Admin\Auth\UsersController@delete')->name('adminUsers.delete');
        });
        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'Admin\Auth\RolesController@index')->name('roles');
            Route::get('/datatableList', 'Admin\Auth\RolesController@datatableList')->name('rolesDatatableList');
            Route::get('/create/', 'Admin\Auth\RolesController@create')->name('roles.create');
            Route::get('/edit/{id}', 'Admin\Auth\RolesController@edit')->name('roles.edit');
            Route::get('/view/{id}', 'Admin\Auth\RolesController@view')->name('roles.view');
            Route::post('/save/{id?}', 'Admin\Auth\RolesController@save')->name('roles.save');
            Route::get('/delete/{id}', 'Admin\Auth\RolesController@delete')->name('roles.delete');
        });

        Route::group(['prefix' => 'permission'], function () {
            Route::get('/', 'Admin\Auth\PermissionsController@index')->name('permissions');
            Route::get('/datatableList', 'Admin\Auth\PermissionsController@datatableList')->name('permissionsDatatableList');
            Route::get('/create/', 'Admin\Auth\PermissionsController@create')->name('permissions.create');
            Route::get('/edit/{id}', 'Admin\Auth\PermissionsController@edit')->name('permissions.edit');
            Route::get('/view/{id}', 'Admin\Auth\PermissionsController@view')->name('permissions.view');
            Route::post('/save/{id?}', 'Admin\Auth\PermissionsController@save')->name('permissions.save');
            Route::get('/delete/{id}', 'Admin\Auth\PermissionsController@delete')->name('permissions.delete');
        });
    });
});

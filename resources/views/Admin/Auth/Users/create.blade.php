@extends('Admin.layout.layout')

@section('content')
    <div class="row">
        <div class="col-12">
            <form class="" method="post" id="UserForm" action="{{ isset($user) ?route('adminUsers.save',$user->id) :route('adminUsers.save') }}" enctype="multipart/form-data">
               {{ csrf_field() }}
                <!-- Default box -->
                <div class="card card-header-custom-color">
                    <div class="card-header">
                        <h3 class="card-title">{{ isset($user) ? 'Edit':'Create' }} User</h3>

                        <div class="card-tools">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Role</label>
                            <select required class="form-control select2bs4" multiple name="roles[]">
                                <option value="">Choose Role</option>
                                @foreach($roles as $id=> $name)
                                    <option value="{{ $id }}" {{ isset($user->role_ids) && in_array($id,$user->role_ids)  ?'selected':'' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                           <div class="form-group">
                               <label for="">Name</label>
                               <input required type="text" name="name" class="form-control" value="{{ isset($user) ? $user->name:old('name') }}" id="name" placeholder="Enter Name">
                           </div>
                        <div class="form-group">
                            <label for="">Username</label>
                            <input required type="text" name="username" class="form-control {{ $errors->first('username') ?'is-invalid':'' }}" value="{{ isset($user) ? $user->username:old('username') }}" id="username" placeholder="Enter Username">

                                <span  class="error invalid-feedback">{{ $errors->first('username') }}</span>

                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input required type="email" name="email" class="form-control {{ $errors->first('email') ?'is-invalid':'' }}" value="{{ isset($user) ? $user->email:old('email') }}" id="email" placeholder="Enter Email">

                                    <span  class="error invalid-feedback">{{ $errors->first('email') }}</span>

                        </div>
                        <div class="form-group">
                            <label for="">Bio</label>
                            <textarea name="bio" class="form-control" placeholder="Enter Bio">{{ isset($user) ? $user->bio: old('bio') }}</textarea>
                        </div>
                       <div class="form-group">
                           <input type="hidden" name="old_image_url" value="{{ isset($user) ? $user->image_url:'' }}">
                               <label>Choose Profile Image</label>
                               <br>
                               <div class="file btn btn-sm btn-primary" style="background-color: #dc3545 !important;">
                                   Choose File
                                   <input type="file" id="profileImage" name="image"/>
                               </div>

                               <div class="image-div" id="imagePreview" style="{{ !isset($user) ?'display: none;' :'' }}">
                                   <br>
                                   <img src="{{ isset($user) ? $user->image_url :'' }}" alt="Avatar" class="custom-image" style="width:100%">
                                   <div class="middle">
                                       <button class="custom-img-text btn btn-xs btn-danger" type="button" onclick="removeIMG($(this))"><i class="fa fa-trash"></i></button>
                                   </div>
                               </div>
                       </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input @if(!isset($user)) required @endif type="password" name="password" class="form-control"  id="password" placeholder="Enter Password">
                            @if(isset($user))<em>Enter password  If you have to change password </em>@endif
                        </div>
                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input @if(!isset($user)) required @endif type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Enter Confirm Password">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary-custom">Submit</button>
                    </div>
                </div>
                <!-- /.card -->
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4',
                placeholder:'Select Roles',
            }).on('change',function(e){
                $(this).valid();
            });
            $('#UserForm').validate({
                rules: {
                    'roles[]':{
                        minlength:1
                    },
                    password: {
                        minlength:6

                    },
                    cpassword:{
                        equalTo:'#password',
                    }
                },
                messages: {
                    name: {
                        required: "Please Enter Name",
                    },
                    username: {
                        required: "Please Enter Username",
                    },
                    email: {
                        required: "Please Enter Email",
                    },
                    password:{
                        required:'Please Enter Password',
                    },
                    cpassword:{
                        required:'Please Enter Confirm Password',
                        equalTo:'Password does not matched',
                    },
                    'roles[]':{
                        required:'Please Select Roles',
                    },
                },
                errorElement: 'span',

            });
            $('#profileImage').on('change',function(){
                var imageUrl =  readURL(this,$('#imagePreview').find('img'));

                $('#imagePreview').show();
            });
        });
        function removeIMG(ele) {
            ele.parents('.image-div').find('.custom-image').attr('src','');
            ele.parents('.image-div').hide();
            $('input[name="old_image_url"]').val('');
        }
        function readURL(input,previewImageEle) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(previewImageEle).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }
    </script>
@endpush

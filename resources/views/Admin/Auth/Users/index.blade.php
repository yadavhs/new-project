@extends('Admin.layout.layout')
@section('content')
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-header-custom-color">
                    <div class="card-header">
                        <h3 class="card-title">Users List</h3>
                        <div class="card-tools">
                            @if(checkPermission('admin.users.create'))
                                <a href="{{ route('adminUsers.create') }}" class="btn btn-sm btn-black"><i class="fa fa-plus"></i>&nbsp; Add</a>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                       <div class="table-responsive">
                           <table class="table table-bordered table-hover table-striped" id="usersList">
                               <thead>
                                   <tr class="text-center">
                                       <th>ID</th>
                                       <th style="width: 20%;">Name</th>
                                       <th>Username</th>
                                       <th>Email</th>
                                       @if(checkPermission('admin.users.view') || checkPermission('admin.users.edit') || checkPermission('admin.users.delete'))
                                           <th>Action</th>
                                       @endif
                                   </tr>

                               </thead>
                               <tbody class="text-center">

                               </tbody>
                           </table>
                       </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">

                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            </div>
        </div>
@endsection
@push('scripts')
<script>
    var user_table;
    $(document).ready(function(e){
        user_table =   $('#usersList').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('adminUserDatatableList') }}",
            "columns" :[
                { "data": "id" },
                { "data": "name" },
                { "data": "username" },
                { "data": "email" },
                @if(checkPermission('admin.users.view') || checkPermission('admin.users.edit') || checkPermission('admin.users.delete'))
                    { "data": "action" }
                @endif

            ],
        });
    });

        function deleteUser(ele,id) {
            // var confirm = ;
            if (confirm('Are you sure you want to delete this item?')) {
                $.ajax({
                    url: "{{ url('admin/admin-users/delete') }}/"+id,
                    method: 'get',
                    // data: {
                    //     priority:position,
                    //     category_id:categoryID
                    // },
                    success: function (data) {
                        // franchise_table.ajax.reload();
                        if (data.status) {
                            alertMSG('success','Item Deleted Successfully');
                            user_table.ajax.reload();
                        }else{
                            alertMSG('error','Item not Deleted');
                        }
                    },
                    error: function (xhr) {
                        alertMSG('error','Internal Server Error');
                    }
                });
            }
        }
</script>
@endpush

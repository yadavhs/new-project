@extends('Admin.layout.layout')

@section('content')
    <div class="row">
        <div class="col-12">
            <form class="" method="post" id="permissionForm" action="{{ isset($permission) ?route('permissions.save',$permission->id) :route('permissions.save') }}">
            {{ csrf_field() }}
            <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ isset($permission) ? 'Edit':'Create' }} Permission</h3>

                        <div class="card-tools">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input required type="text" name="name" class="form-control" value="{{ isset($permission) ? $permission->name:'' }}" id="name" placeholder="Enter  Name">
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea required name="description" class="form-control">{{ isset($permission) ? $permission->description:'' }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary-custom">Submit</button>
                    </div>
                </div>
                <!-- /.card -->
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#permissionForm').validate({

                messages: {
                    name: {
                        required: "Please Enter Name",
                    },
                    description:{
                        required: "Please Enter Description",
                    }
                },
                errorElement: 'span',
            });
        });
    </script>
@endpush
